#ifndef LISTENER_H
#define LISTENER_H

#include "ws_socket_io.h"

#include <boost/beast/http/file_body.hpp>

// Accepts incoming connections and launches the sessions
class listener : public std::enable_shared_from_this<listener>, fail_impl {
    net::io_context& ioc_;
    tcp::acceptor acceptor_;
    WSServerClient &socket_container;

public:
    listener(net::io_context& ioc, tcp::endpoint endpoint, WSServerClient &socket_io) : ioc_(ioc), acceptor_(ioc), socket_container(socket_io) {
        beast::error_code ec;

        // Open the acceptor
        acceptor_.open(endpoint.protocol(), ec);
        if(ec) {
            fail(ec, "open");
            return;
        }

        // Allow address reuse
        acceptor_.set_option(net::socket_base::reuse_address(true), ec);
        if(ec) {
            fail(ec, "set_option");
            return;
        }

        // Bind to the server address
        acceptor_.bind(endpoint, ec);
        if(ec) {
            fail(ec, "bind");
            return;
        }

        // Start listening for connections
        acceptor_.listen(net::socket_base::max_listen_connections, ec);
        if(ec) {
            fail(ec, "listen");
            return;
        }
    }

    // Start accepting incoming connections
    void run() {
        do_accept();
    }

private:
    boost::beast::http::request_parser<boost::beast::http::file_body> header_parser_;
    boost::beast::flat_buffer buffer{ 8192 };

    void do_accept() {
        // The new connection gets its own strand
        acceptor_.async_accept(net::make_strand(ioc_), beast::bind_front_handler(&listener::on_accept, shared_from_this()));
    }

    void on_accept(beast::error_code ec, tcp::socket socket){
        if(ec) {
            fail(ec, "accept 3");
        }
        else {
            // Create the session and run it

//            // Читаем заголовок
//            boost::beast::http::read_header(socket, buffer, header_parser_, ec);
//            if (ec) std::cout << "on_accept on read header error" << std::endl;
//            // Берем поле Id из заголовка
//            std::cout << header_parser_.get().at("Id").to_string() << std::endl;

            auto newsession = std::make_shared<session>(std::move(socket));
            std::cout << "70 str" << std::endl;
            socket_container.addNewSession(newsession);
        }

        // Accept another connection
        do_accept();
    }

//    void on_read_header(boost::system::error_code ec, std::size_t bytes_transferred) {
//        boost::ignore_unused(bytes_transferred);
//        if (ec) std::cout << "on read header error" << std::endl;

//        std::cout << header_parser_.get().at("Id").to_string() << std::endl;
//        if (header_parser_.is_done()) std::cout << "is done" << std::endl;
//        if (header_parser_.is_header_done())  std::cout << "is header done" << std::endl;
//    }
};

#endif // LISTENER_H
