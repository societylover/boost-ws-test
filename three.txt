{"orders": [{
	"guid": "123",
	"number": "33",
	"deliver": "2018-01-25T11:21:31",
	"phone": "9880123122",
	"address": "Ленина 33",
	"lat": 46.348296,
	"lon": 48.045155,
	"courier": false,
	"amount": 1300.0,
	"state": "подготовлен",
	"positions": [
		{
		"item": "шашлык",
		"quantity": 300.0,
		"amount": 3.0,
		"cooked": true
		},
		{
		"item": "люля-кебаб",
		"quantity": 100.0,
		"amount": 3.0,
		"cooked": false
		},
		{
		"item": "пельмени",
		"quantity": 400.0,
		"amount": 11.0,
		"cooked": true
		}
	]
},
{
	"guid": "333",
	"number": "34",
	"deliver": "2018-01-25T15:21:31",
	"phone": "9880123120",
	"address": "Бакинская 10",
	"lat": 46.339335132188594, 
	"lon": 48.01743245392331,
	"courier": false,
	"amount": 2300.0,
	"state": "принят",
	"positions": [
		{
		"item": "пистолетики",
		"quantity": 300.0,
		"amount": 3.0,
		"cooked": true
		},
		{
		"item": "люля-кебаб",
		"quantity": 100.0,
		"amount": 3.0,
		"cooked": false
		},
		{
		"item": "хлеб",
		"quantity": 400.0,
		"amount": 11.0,
		"cooked": true
		}
	]
},
{
	"guid": "111",
	"number": "35",
	"deliver": "2018-01-26T17:21:31",
	"phone": "9880123124",
	"address": "Донбасская 41",
	"lat": 46.340847,
	"lon": 48.021615,
	"courier": true,
	"amount": 3300.0,
	"state": "отправлен",
	"positions": [
		{
		"item": "хлеб",
		"quantity": 300.0,
		"amount": 5.0,
		"cooked": true
		},
		{
		"item": "люля-кебаб",
		"quantity": 100.0,
		"amount": 2.0,
		"cooked": false
		},
		{
		"item": "кока-кола",
		"quantity": 400.0,
		"amount": 11.0,
		"cooked": true
		}
	]
}
]
}