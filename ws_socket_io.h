#ifndef WS_SOCKET_IO_H
#define WS_SOCKET_IO_H

#include <vector>
#include <iostream>
#include "session.h"

#include <fstream>

// сохраняем сессии
// выбираем сессию для отправки сообщения
// выбираем файл с сообщением
class WSServerClient {

public:
    WSServerClient() = default;

    void addNewSession(std::shared_ptr<session> newSession) {
        sessions.push_back(newSession);
        newSession->run();
        //if (sessionN == 2) writeToSession();
    }

    std::string getFileValue(std::string &fname) {
        std::ifstream file(fname);
        return { std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>() };
    }

    void writeToSession() {
        while(true)
        if (!sessions.empty()) {
            uint16_t sessionN = 0;
            std::cout << "\nНомер сессии " << "(0 - " << sessions.size()-1 << "): ";
            std::cin >> sessionN;
            std::cout << "\n";
            auto session = sessions[sessionN];
            std::cout << "Файл: 0 - one.txt, 1 - three.txt: ";
            std::cin >> sessionN;
            std::cout << "\n";
            auto value = getFileValue(files[sessionN]);
            (*session).do_write(value);
        }
    }

private:
    int sessionN = 0;
    std::vector<std::shared_ptr<session>> sessions;
    std::string files[2] = {"one.txt", "three.txt"};
};

#endif // WS_SOCKET_IO_H
