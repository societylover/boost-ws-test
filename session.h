#ifndef SESSION_H
#define SESSION_H

//
// Copyright (c) 2016-2019 Vinnie Falco (vinnie dot falco at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//

//------------------------------------------------------------------------------
//
// Example: WebSocket server, asynchronous
//
//------------------------------------------------------------------------------

#include "fail.h"

//------------------------------------------------------------------------------
#include <boost/beast/http/file_body.hpp>

// Echoes back all received WebSocket messages
class session : public std::enable_shared_from_this<session>, fail_impl {
    websocket::stream<beast::tcp_stream> ws_;
    beast::flat_buffer buffer_;

public:
    // Take ownership of the socket
    explicit
    session(tcp::socket&& socket) : ws_(std::move(socket)) {
    }

    // Get on the correct executor
    void run() {

        std::cout << "session run 38" << std::endl;

        // We need to be executing within a strand to perform async operations
        // on the I/O objects in this session. Although not strictly necessary
        // for single-threaded contexts, this example code is written to be
        // thread-safe by default.
        net::dispatch(ws_.get_executor(), beast::bind_front_handler(&session::on_run, shared_from_this()));
    }

    // Start the asynchronous operation
    void on_run() {
        std::cout << "session on_run 49" << std::endl;

        // Set suggested timeout settings for the websocket
        ws_.set_option(websocket::stream_base::timeout::suggested(beast::role_type::server));

        // Set a decorator to change the Server of the handshake
        ws_.set_option(websocket::stream_base::decorator([](websocket::response_type& res) {
                     res.set(http::field::server,
                     std::string(BOOST_BEAST_VERSION_STRING) +  " websocket-server-async"); }));

        // Accept the websocket handshake
        ws_.async_accept(beast::bind_front_handler(&session::on_accept,shared_from_this()));
    }

    // error when ws closing
    void on_accept(beast::error_code ec) {
        if (ec) return fail(ec, "accept on_accept");
        // Read a message
        do_read();
    }

    void do_read() {
        // Read a message into our buffer
        beast::error_code ec;
        ws_.async_read(buffer_, beast::bind_front_handler(&session::on_read, shared_from_this()));
    }

    void on_read(beast::error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);
        // This indicates that the session was closed
        if(ec == websocket::error::closed) return;

        if(ec) fail(ec, "read");
        // Echo the message
        ws_.text(ws_.got_text());

        ws_.async_write(buffer_.data(), beast::bind_front_handler(&session::on_write, shared_from_this()));
        // do_write("123");
    }

    void do_write(const std::string &value) {
        buffer_.clear();
        size_t n = buffer_copy(buffer_.prepare(value.length()), boost::asio::buffer(value));
        buffer_.commit(n);
        // buffer_.data().data() = value.c_str();

        ws_.async_write(buffer_.data(), beast::bind_front_handler(&session::on_write, shared_from_this()));
    }

    void on_write(beast::error_code ec, std::size_t bytes_transferred) {

        boost::ignore_unused(bytes_transferred);

        if(ec) return fail(ec, "write");
        // Clear the buffer
        buffer_.consume(buffer_.size());

        // Do another read
        do_read();
    }
};


#endif // SESSION_H
